pragma solidity >=0.7.3;

import "./SafeMath.sol";
import "./Ownable.sol";

contract Event is Ownable {
    using SafeMath for uint256;

    uint256 public id;
    string public name;
    uint256 public date;
    string public description;
    uint256 public threshold;
    uint64 public addresses_capacity;
    uint256 public profitSharing;
    
    bool public alreadyReturnNonViableStake;

    //address1 = got verified, address2 = verifyFrom, uint = amount
    mapping (address => address[]) public stakingAddress;
    address[] private addresses;
    mapping (address => uint256[]) public stakingAmount;

    uint256 public totalStake;

    constructor(uint256 _id, string memory _name, uint256 _date, string memory _description, uint256 _threshold, uint64 _addresses_capacity) {
        id = _id;
        name = _name;
        date = _date;
        description = _description;
        threshold = _threshold;
        addresses_capacity = _addresses_capacity;
    }

    function getName() public view returns (string memory) {
        return name;
    }
    
    function addProfitSharing() public onlyOwner payable {
        profitSharing = profitSharing.add(msg.value);
    }
    
    function contractClose() public onlyOwner {
        selfdestruct(payable(owner()));
    }

    function stake(address _address) public payable stakeable {
        stakingAddress[_address].push(msg.sender);
        stakingAmount[_address].push(msg.value);

        if (!doesAddressesContainAddress(_address)) {
            addresses.push(_address);
        }
        
        totalStake = totalStake.add(msg.value);
    }

    function doesAddressesContainAddress(address _address) private view returns(bool) {
        for (uint256 i = 0; i < addresses.length; i++) {
            if (addresses[i] == _address) {
                return true;
            }
        }
        return false;
    }

    function withinStakeTime() public view returns (bool) {
        return (date.sub(3*24*60*60)) <= block.timestamp && block.timestamp <= date;
    }

    function addressCapNotExceed() public view returns (bool) {
        return addresses.length < addresses_capacity;
    }

    modifier stakeable() {
        require(withinStakeTime(), "Not stakeable according to timestamp");
        require(addressCapNotExceed(), "Not stakeable due to full capacity");
        _;
    }

    function isExpired() public view returns (bool) {
       uint256 expriedDate = date.add(14*24*60*60);
       return block.timestamp >= expriedDate;
    }

    function shouldRunReturnNonViableStake() public view returns (bool) {
        return !alreadyReturnNonViableStake && block.timestamp >= date;
    }

    function returnNonViableStake() /**onlyOwner**/ public {
        for (uint256 i = 0; i < addresses.length; i++){
            address addr = addresses[i];
            // find total stake of that joiner
            uint256 total_stake = 0;
            for (uint256 j = 0; j < stakingAmount[addr].length; j++) {
                total_stake = total_stake.add(stakingAmount[addr][j]);
            }

            if (total_stake < threshold) {
                refundToStakerOfJoiner(addr);
                delete stakingAddress[addr];
                delete stakingAmount[addr];
                
                removeAddresses(i);
                
                totalStake = totalStake.sub(total_stake);
            }
        }
        alreadyReturnNonViableStake = true;
    }

    function removeAddresses(uint256 index) private {
         for (uint256 i = index; i < addresses.length-1; i++) {
            addresses[i] = addresses[i+1];
        }
        delete addresses[addresses.length-1];
    }

    function resolve() /**onlyOwner**/ public {
        for (uint256 i = 0; i < addresses.length; i++){
            resolve(addresses[i]);
        }
        
    }

    function resolve(address _address) private {
        for (uint256 i = 0; i < stakingAddress[_address].length; i++){
            uint256 myStake = stakingAmount[_address][i];
            uint256 amount = myStake.add(profitSharing.mul(myStake.div(totalStake)));
            payable(stakingAddress[_address][i]).transfer(amount);
        }
    }
    

    function refundToStakerOfJoiner(address _address) private {
        for (uint256 i = 0; i < stakingAddress[_address].length; i++){
            payable(stakingAddress[_address][i]).transfer(stakingAmount[_address][i]);
        }
    }

    function report() onlyOwner public {
        for (uint256 i = 0; i < addresses.length; i++){
            report(addresses[i]);
        }
    }

    function report(address _address) private {
        for (uint256 i = 0; i < stakingAddress[_address].length; i++){
            payable(owner()).transfer(stakingAmount[_address][i]);
        }
        payable(owner()).transfer(profitSharing);
    }    

    function verify() public view returns (bool) {
        for (uint256 i = 0; i < addresses.length; i++) {
            if (addresses[i] == msg.sender) {
                return true;
            }
        }
        return false;
    }

    function setId(uint256 _id) public onlyOwner {
        id = _id;
    }
}