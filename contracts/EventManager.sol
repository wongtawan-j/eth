pragma solidity >=0.7.3;

import "./Ownable.sol";
import "./Event.sol";
import "./SafeMath.sol";

contract EventManager is Ownable {
    using SafeMath for uint256;

    Event[] public events;
    mapping(address => address[]) public eventOwnerToEventAddrs;
    mapping(address => address) public eventAddrToEventOwner;

    uint256 public id = 0;
    // 9.4e16 (5,000 THB)
    uint256 public min_profitSharing = 94000000000000000;
    // 4.7e16 (2,500 THB)
    uint256 public threshold = 47000000000000000;    

    function getAddrToEventOwner (address _address) public view returns(address eventOwnerAddrs) {
        return eventAddrToEventOwner[_address];
    }

    function getEventOwnertoEventAddrs (address _address) public view returns (address[] memory eventAddrs){
        return eventOwnerToEventAddrs[_address];
    }
    
    function addEvent (string memory name, uint256 date, string memory description, uint64 addresses_capacity) public payable returns (address) {
        uint256 profitSharing = msg.value;
        require(profitSharing >= min_profitSharing, "Profit sharing not enough");
        Event e = new Event(id, name, date, description, threshold, addresses_capacity);
        e.addProfitSharing{value:msg.value}();
        events.push(e);

        address eventAddr = address(e);
        eventOwnerToEventAddrs[msg.sender].push(eventAddr);
        eventAddrToEventOwner[eventAddr] = msg.sender;

        id = id.add(1);

        return eventAddr;
    }

    function timerTrigger() public onlyOwner returns(uint256 eventRemoved) {
        for (uint256 i = 0; i < events.length; i++){
            if(events[i].shouldRunReturnNonViableStake()) {
                events[i].returnNonViableStake();
            } else if(events[i].isExpired()) {
                address eventAddr = address(events[i]);
                address eventOwner = eventAddrToEventOwner[eventAddr];

                for (uint256 j = 0; j < eventOwnerToEventAddrs[eventOwner].length; j++) {
                    if (eventOwnerToEventAddrs[eventOwner][j] == eventAddr) {
                        removeEventAddressFromEventAddrs(j, eventOwner);
                    }
                }

                delete eventAddrToEventOwner[eventAddr];
                if (eventOwnerToEventAddrs[eventOwner].length == 0) {
                    delete eventOwnerToEventAddrs[eventOwner];
                }

                events[i].resolve();
                events[i].contractClose();
                removeEvent(i);
                eventRemoved += 1;
            }
        }
    }

    function removeEventAddressFromEventAddrs(uint256 index, address eventOwner) private {
        for (uint256 i = index; i < eventOwnerToEventAddrs[eventOwner].length-1; i++) {
            eventOwnerToEventAddrs[eventOwner][i] = eventOwnerToEventAddrs[eventOwner][i+1];
        }
        delete eventOwnerToEventAddrs[eventOwner][eventOwnerToEventAddrs[eventOwner].length-1];
    }

    function editMinProfitSharing(uint256 _min_profitSharing) public onlyOwner {
        min_profitSharing = _min_profitSharing;
    }

    function editMinStake(uint256 _threshold) public onlyOwner {
        threshold = _threshold;
    }

    function removeEvent(uint256 index) private {
        for (uint256 i = index; i < events.length-1; i++) {
            events[i] = events[i+1];
            events[i].setId(i);
        }
        delete events[events.length-1];
    }

    function reportEvent(uint256 eid) public onlyOwner {
        events[eid].contractClose();
        removeEvent(eid);
    }

    function getBalance() public view onlyOwner returns(uint256) {
        return address(this).balance;
    }

    function withdrawBalance(uint256 amt) public onlyOwner {
        require(getBalance() >= amt, "cant withdraw exceed amt");
        payable(owner()).transfer(amt);
    }
}
